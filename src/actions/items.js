export const ACTIONS = {
    SUBMIT_REQUEST: 'SUBMIT_REQUEST'
}

export const submitRequest = (data) => ({
    type: ACTIONS.SUBMIT_REQUEST,
    data
})
