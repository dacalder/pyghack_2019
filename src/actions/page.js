export const ACTIONS = {
    UPDATE_LOADING_STATUS: 'UPDATE_LOADING_STATUS',
    UPDATE_VIEW: 'UPDATE_VIEW'
}

export const VIEWS = {
    INDEX: 'INDEX',
    ITEMS: 'ITEMS',
    REQUEST: 'REQUEST'
}

export const updateLoadingStatus = (isLoading) => ({
    type: ACTIONS.UPDATE_LOADING_STATUS,
    isLoading
})

export const updateView = (view) => ({
    type: ACTIONS.UPDATE_VIEW,
    view
})
