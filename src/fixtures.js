import imageBagpack from './images/fixtures/bagpack.png'
import imageDryer from './images/fixtures/dryer.png'
import imageFridge from './images/fixtures/fridge.png'
import imageFurniture from './images/fixtures/furniture.png'
import imageStationery from './images/fixtures/stationery.png'
import imageWasher from './images/fixtures/washer.png'

export const items = [
    {
        id: 1,
        name: 'Bagpack',
        description: 'School bagpack',
        numberRequested: 3,
        numberAvailable: 10,
        image: imageBagpack
    },
    {
        id: 2,
        name: 'Dryer',
        description: '',
        numberRequested: 2,
        numberAvailable: 0,
        image: imageDryer
    },
    {
        id: 3,
        name: 'Fridge',
        description: 'Small size',
        numberRequested: 6,
        numberAvailable: 2,
        image: imageFridge
    },
    {
        id: 4,
        name: 'Closet',
        description: 'Small and portable',
        numberRequested: 4,
        numberAvailable: 1,
        image: imageFurniture
    },
    {
        id: 5,
        name: 'School Stationery',
        description: 'Pens, pencils, notebooks, etc.',
        numberRequested: 50,
        numberAvailable: Infinity,
        image: imageStationery
    },
    {
        id: 6,
        name: 'Washer',
        description: '',
        numberRequested: 4,
        numberAvailable: 2,
        image: imageWasher
    }
]

export const admins = []
