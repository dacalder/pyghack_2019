import { fromJS } from 'immutable'

import { ACTIONS, VIEWS } from '../actions/page'

const INITIAL_STATE = fromJS({
    isLoading: false,
    view: VIEWS.INDEX
})

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTIONS.UPDATE_LOADING_STATUS:
            return state.set('isLoading', action.isLoading)
        case ACTIONS.UPDATE_VIEW:
            return state.set('view', action.view)
        default:
            return state
    }
}
