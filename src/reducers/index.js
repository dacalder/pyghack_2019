import items from './items'
import page from './page'

export default {
    items,
    page
}
