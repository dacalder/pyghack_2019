import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import { submitRequest } from '../actions/items'

const BaseRequest = ({ submit }) => (
    <div>
        Request
    </div>
)

BaseRequest.prototype = {
    submit: PropTypes.func.isRequired
}
const Request = connect(null, { submit: submitRequest })(BaseRequest)

export default Request
