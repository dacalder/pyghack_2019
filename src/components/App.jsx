import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { VIEWS } from '../actions/page'
import Index from './Index'
import Items from './Items'
import Request from './Request'

const BaseApp = ({ view }) => {
    switch (view) {
        case VIEWS.ITEMS:
            return <Items />
        case VIEWS.REQUEST:
            return <Request />
        case VIEWS.INDEX:
        default:
            return <Index />
    }
}

BaseApp.propTypes = {
    view: PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({
    view: state.getIn(['page', 'view'])
})

const App = connect(mapStateToProps)(BaseApp)

export default App
