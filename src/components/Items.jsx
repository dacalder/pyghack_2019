import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Card, Icon, Image, Segment, Container } from 'semantic-ui-react'

const BaseItems = ({ items }) => (
    <Segment basic padded>
        <Container fluid textAlign="center">
            <Card.Group>
                {items.map(({ id, name, description, numberRequested, numberAvailable, image }) => (
                    <Card key={id}>
                        <Image src={image} wrapped ui={false} />
                        <Card.Content>
                            <Card.Header>
                                {name}
                            </Card.Header>
                            <Card.Description>
                                {description}
                            </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            Requested: {numberRequested}
                            {numberAvailable > numberRequested ?
                                <Icon name="check circle outline" color="green" /> :
                                <Icon name="warning circle" color="yellow" />}
                            Available: {numberAvailable}
                        </Card.Content>
                    </Card>
                ))}
            </Card.Group>
        </Container>
    </Segment>
)

BaseItems.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({})).isRequired
}

const mapStateToProps = (state) => ({
    items: state.getIn(['items', 'items']).toJS()
})

const Items = connect(mapStateToProps)(BaseItems)

export default Items
