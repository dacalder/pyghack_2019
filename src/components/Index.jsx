import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Container, Header, Segment } from 'semantic-ui-react'

import { updateView, VIEWS } from '../actions/page'

const BaseIndex = ({ changeView }) => (
    <Segment basic padded>
        <Container fluid textAlign="center">
            <Header>Champaign County Resource Pool</Header>
            <Segment basic>
                <Button primary content="Request an Item" icon="add" onClick={() => { changeView(VIEWS.REQUEST) }} />
                <Button primary content="See List of Items in Need" icon="list" onClick={() => { changeView(VIEWS.ITEMS) }} />
            </Segment>
        </Container>
    </Segment>
)

BaseIndex.propTypes = {
    changeView: PropTypes.func.isRequired
}

const Index = connect(null, { changeView: updateView })(BaseIndex)

export default Index
